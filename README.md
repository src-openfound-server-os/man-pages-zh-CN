NFSChina Server OS
=
代码仓库说明
-
**分支说明**
* 进行代码开发工作时，请注意选择当前版本对应的分支
* NFS-X.0分支为对应版本的主分支，如NFS-4.0分支对应4.0的版本

开发流程
-
1. 首先fork目标分支到自己的namespace
2. 在自己的fork分支上做出修改
3. 向对应的仓库中提交merge request，源分支为fork分支

开发指南
-
**Fork 分支：**
* 打开目标项目，选择fork，选择目标分支，选择自己的namespace，点击确定，等待完成

**Git 全局设置：**
* git config --global user.name "用户名xxx"
* git config --global user.email "xxx@nfschina.com"

**克隆 git 仓库：**
* git clone https://gitee.com/xxx.git
 
**修改提交 git 仓库：**
* cd xxx
* git checkout -b NFS4.0-Devel
* touch README.md
* git add README.md
* git commit -m "add README"
* git push origin NFS4.0-Devel

**提交 Pull Request（合并请求）：**
* 打开目标仓库地址，如：https://gitee.com/xxx.git
* 点击 `+ Pull Request`
* 选择`源分支`，选择`目标分支`
* 填入标题、说明，关联pr、Issue等
* 临时保存`创建 Pull Request 草稿`，发起合并请求`创建 Pull Request`